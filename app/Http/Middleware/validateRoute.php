<?php

namespace App\Http\Middleware;

use Closure;
use GuzzleHttp\Client;

class validateRoute
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $sub_domain = request()->getHost();
        // $sub_domain = $request->name;

        $client = new Client(); //GuzzleHttp\Client
        try {
            $result = $client->request('post', env('RVD'), [
                'json' => [
                    'sub_domain' => $sub_domain,
                ],
            ]);
            $response = json_decode($result->getBody());
            config(['rainy.tenantid' => $response->tenantId]);
            config(['rainy.tenantname' => $response->tenantName]);
        } catch (\Exception $e) {
            // Handle exception later
            return response()->view('errors.missing', [], 404);
        }

        return $next($request);
    }
}
