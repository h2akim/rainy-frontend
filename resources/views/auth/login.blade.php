@extends('layouts.app')

@section('css')
<link href="{{ asset('css/login.css') }}" rel="stylesheet">
<link href="{{ asset('css/animate.css') }}" rel="stylesheet">
@endsection

@section('content')
<div class="container-fluid" style="margin-bottom: 70px;">
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-form">
                <img src="/images/p_rainy.svg" class="login-logo animated jackInTheBox" alt="">
            </div>

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <form class="login-input-form animated fadeIn" method="POST" action="">
              {{ csrf_field() }}
              <div class="form-group">
                <label for="email">Email address:</label>
                <input name="email" type="email" class="form-control" id="email">
              </div>
              <div class="form-group">
                <label for="pwd">Password:</label>
                <input name="password" type="password" class="form-control" id="pwd">
              </div>
              <div class="checkbox">
                <label><input type="checkbox"> Remember me</label>
              </div>
              <button type="submit" class="btn btn-default">Login</button>
            </form>
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
