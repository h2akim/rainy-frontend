@extends('layouts.app')

@section('css')
<link href="{{ asset('packages/bootstrap-notifications-master/dist/stylesheets/bootstrap-notifications.min.css') }}" rel="stylesheet" type="text/css">
<link href="https://cdn.quilljs.com/1.3.1/quill.snow.css" rel="stylesheet">
@endsection

@section('content')
	<notifications></notifications>
	<Navbar v-if="$route.path !== '/login'"></Navbar>
    <router-view></router-view>
@endsection
