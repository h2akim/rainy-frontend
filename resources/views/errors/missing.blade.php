<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>404</title>
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <style media="screen">
            html, body {
                width: 100%;
                height: 100%;
                padding: 0;
                margin: 0;
            }

            body {

                background: #73C8A9;  /* fallback for old browsers */
                background: -webkit-linear-gradient(to top, #373B44, #73C8A9);  /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to top, #373B44, #73C8A9); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

            }

            .parent {
                position: relative;
            }

            .child {
                position: absolute;
                top: 50%;
                left: 50%;
                transform: translate(-50%, 50%);
            }

        </style>
    </head>
    <body>
        <div class="parent">
                <img src="/images/logo_cry.svg" alt="" class="child" data-trigger="manual" data-toggle="tooltip" title="Help me. I'm lost.">
        </div>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip("show");
        });
        </script>
    </body>
</html>
