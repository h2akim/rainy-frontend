import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const router = new VueRouter({
	mode: 'history',
	routes: [
        { path: '/login', component: require('./components/users/Login.vue') },
		{ path: '/', component: require('./components/Dashboard.vue'), meta: { requireAuth: true } },
		{
			path: '/settings',
			component: require('./components/Settings.vue'),
			children: [
                {
                    path: '',
                    component: require('./components/settings/Profile.vue'),
                    meta: {
                        requireAuth: true
                    }
                },
                {
                    path: 'account',
                    component: require('./components/settings/Account'),
                    meta: {
                        requireAuth: true
                    }
                },
                {
                    path: 'log',
                    component: require('./components/settings/Log'),
                    meta: {
                        requireAuth: true
                    }
                },
				{
					path: 'notifications',
					component: require('./components/settings/Notifications'),
                    meta: {
                        requireAuth: true
                    }
				}
            ],
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/tickets',
            name: 'overview',
            component: require('./components/tickets/overviews/OverviewNew.vue'),
            children: [
                {
                    path: 'assigned',
                    name: 'ticket-assigned',
                    component: require('./components/tickets/overviews/Assigned.vue'),
                    meta: {
                        requireAuth: true
                    }
                },
                {
                    path: 'open',
                    name: 'ticket-open',
                    component: require('./components/tickets/overviews/Open.vue'),
                    meta: {
                        requireAuth: true
                    }
                },
                {
                    path: 'closed',
                    name: 'ticket-closed',
                    component: require('./components/tickets/overviews/Closed.vue'),
                    meta: {
                        requireAuth: true
                    }
                },
                {
                    path: 'all',
                    component: require('./components/tickets/overviews/All.vue'),
                    name: 'ticket-all',
                    meta: {
                        requireAuth: true,
                        title: 'Chitcat - Tickets - All Tickets'
                    }
                },
                {
                    path: '/tickets/view/:id',
                    name: 'view-ticket',
                    component: require('./components/tickets/overviews/View.vue'),
                    meta: {
                        requireAuth: true
                    }
                },

				{
                    path: '/tickets/create',
                    name: 'create-ticket',
                    component: require('./components/tickets/Create.vue'),
                    meta: {
                        requireAuth: true
                    }
                },

            ],
            meta: {
                requireAuth: true
            }
        },
        {
            path: '/admin',
            component: require('./components/admin/Settings.vue'),
            children: [
                {
                    path: '',
                    component: require('./components/admin/Managements.vue'),
                    meta: {
                        requireAuth: true,
                        title: 'Chitcat - Admin Settings'
                    }
                },
                {
                    path: 'accounts',
                    component: require('./components/admin/Accounts.vue'),
                    meta: {
                        requireAuth: true,
                        title: 'Chitcat - Admin Settings - User Accounts'
                    }
                },
                {
                    path: 'billing',
                    component: require('./components/admin/Billing.vue'),
                    meta: {
                        requireAuth: true,
                        title: 'Chitcat - Admin Settings - Billing'
                    }
                },
                {
                    path: 'clients',
                    component: require('./components/admin/Clients.vue'),
                    meta: {
                        requireAuth: true,
                        title: 'Chitcat - Admin Settings - Clients'
                    }
                },
            ],
            meta: {
                requireAuth: true
            }
        },
	],
	linkActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
    if (to.meta.title !== undefined)
        document.title = to.meta.title
    else
        document.title = 'Chitcat'

    if (!window.localStorage.getItem('auth') && to.meta.requireAuth) {
        window.location = '/login'
    } else if (window.localStorage.getItem('auth') && to.path === '/login') {
        window.location = '/'
    } else {
        next()
    }
})

export default router
