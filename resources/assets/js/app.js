/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

/* requires */
require('./bootstrap');
window.Vue = require('vue');
require('vue-resource');
require('./components');

/* imports */
import router from './routes'
import store from './store/'
import Notify from 'vue-notifyjs'
import Vue2Filters from 'vue2-filters'
import moment from 'moment'
import VueClip from 'vue-clip'
import Vddl from 'vddl';
import VueSweetAlert from 'vue-sweetalert'
import VueEmoji from 'rui-vue-emoji'
/* Uses */


Vue.use(VueSweetAlert)
Vue.use(Vddl);
Vue.use(Notify)
Vue.use(Vue2Filters)
Vue.use(VueClip);
// Vue.use(VueEmoji);
/* Prototypes */
Vue.prototype.moment = moment
Vue.directive('tooltip', function(el, binding) {
  $(el).tooltip({
    title: binding.value,
    placement: binding.arg,
    trigger: 'hover'
  })
})
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  store,
  el: '#app',
  router
});
