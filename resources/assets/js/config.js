export const api = 'http://rainy-api.dev/api/v1/';

export const endpoints = {
	auth: {
		login: api + 'Authentication/authenticate',
		logout: api + 'Authentication/logout'
	},
	TenantService: {
		getInfo: api + 'TenantServices/getInfo'
	},
	AccountServices: {
		getProfile: api + 'AccountServices/getProfile',
		putProfile: api + 'AccountServices/putProfile',
		putPassword: api + 'AccountServices/putPassword'
	},
	TicketServices: {
		getTickets: api + 'TicketServices/getTickets', // list
		getTicket: api + 'TicketServices/getTicket/full', // view
		getTicketReplies: api + 'TicketServices/getTicket/replies', // reply
		postAddTicket: api + 'TicketServices/postAddTicket', // post
		putTicket: api + 'TicketServices/putTicket', // put
		postReply: api + 'TicketServices/postReply', // reply
		Comments: {
			postComment: api + 'TicketServices/Comments/postComment'
		}
	},
	TagServices: {
		getTags: api + 'TagServices/getTags',
		fetchTags: api + 'TagServices/fetchTags',
		fetchTagGroups: api + 'TagServices/fetchTagGroups',
		addTag: api + 'TagServices/addTag',
		deleteLabel: api + 'TagServices/deleteLabel',
		toggleTag: api + 'TagServices/toggleTag',
		addLabelGroup: api + 'TagServices/addLabelGroup',
		deleteLabelGroup: api + 'TagServices/deleteLabelGroup',
	},
	UserServices: {
		getAgents: api + 'UserServices/getAgents',
		getAgent: api + 'UserServices/getAgent',
		update: api + 'UserServices/putUser',
		add: api + 'UserServices/addUser'
	},
	StatusServices: {
		getStatuses: api + 'StatusServices/getStatuses'
	},
	ClientServices: {
		getClients: api + 'ClientServices/getClients',
		allClients: api + 'ClientServices/allClients',
		postClient: api + 'ClientServices/postClient',
		toggleClient: api + 'ClientServices/toggleClient',
		deleteClient: api + 'ClientServices/deleteClient',
		allOrganizations: api + 'ClientServices/allOrganizations'
	},
	SettingServices: {
		getSettings: api + 'SettingServices/getSettings',
		putSettings: api + 'SettingServices/putSettings'
	}
}

export const getHeaders = function () {
	const tokenData = JSON.parse(window.localStorage.getItem('auth'))
	const headers = {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
		'Authorization': 'Bearer ' + tokenData.token,
		'tenantId': window.Rainy.tenantId
	}
	return headers
}

export const setNewToken = function (newToken) {
	var tokenData = JSON.parse(window.localStorage.getItem('auth'))
	tokenData.token = newToken
	window.localStorage.setItem('auth', JSON.stringify(tokenData))
}

export const saveToken = function (response, tenantId) {
	const auth = {
		token: response.data.token,
		exp: response.data.exp,
		tenantId: tenantId,
		user: response.data.user
	};
	window.localStorage.setItem('auth', JSON.stringify(auth));
}

export const currentUser = function () {
	var $parse = JSON.parse(window.localStorage.getItem('auth'));
	return $parse.user;
}

export const dumpAuth = function () {
	window.localStorage.removeItem('auth');
}
