// Components

Vue.component('Navbar', require('./components/partials/Navbar.vue'));

// Ticket View Components
Vue.component('TicketDetails', require('./components/tickets/overviews/ticket-components/Details.vue'));
Vue.component('TicketReplies', require('./components/tickets/overviews/ticket-components/Replies.vue'));
Vue.component('addcomment', require('./components/tickets/overviews/ticket-components/Comment.vue'));
Vue.component('inlineemail', require('./components/tickets/overviews/ticket-components/InlineEmail.vue'));

// Label / Tags
Vue.component('tag-managements', require('./components/admin/managements/Tags.vue'));
Vue.component('add-tag', require('./components/admin/managements/AddTag.vue'));
Vue.component('tagList', require('./components/admin/managements/tagList.vue'));

// Admin setting -> organizations
Vue.component('organization', require('./components/admin/managements/Organization.vue'));

// Admin Setting Clients
Vue.component('clientOrganization', require('./components/admin/clients/Organizations.vue'));
Vue.component('clients', require('./components/admin/clients/Clients.vue'));

// Notifications
Vue.component('notification', require('./components/notifications/Notification.vue'));
