import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions'
import * as getters from './getters'
import ticket from './modules/ticket'
import tags from './modules/tags'

Vue.use(Vuex)

export default new Vuex.Store({
	getters,
	actions,
	modules: {
		ticket,
		tags
	}
})