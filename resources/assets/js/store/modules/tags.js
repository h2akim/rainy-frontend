import {
  endpoints,
  getHeaders
} from '../../config'

const state = {
  tags: [],
  tagGroups: []
}

const getters = {

  allTags: state => state.tags,
  tagGroups: state => state.tagGroups

}

const actions = {

  fetchTags({
    commit
  }, group_id) {

    axios.get(endpoints.TagServices.fetchTags + '/' + group_id, {
        headers: getHeaders()
      })
      .then((response) => {
        commit('populateTag', response.data.items)
      })
      .catch((errors) => {
        console.log(errors)
      })

  },

  fetchTagGroups({
    commit
  }) {

    axios.get(endpoints.TagServices.fetchTagGroups, {
        headers: getHeaders()
      })
      .then((response) => {
        commit('populateTagGroup', response.data.groups)
        console.log(response)
      })
      .catch((errors) => {
        console.log(errors)
      })

  }

}

const mutations = {

  populateTag(state, tags) {
    state.tags = tags
  },

  populateTagGroup(state, groups) {
    state.tagGroups = groups
    console.log(state.tagGroups)
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
