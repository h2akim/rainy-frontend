import {
  endpoints,
  getHeaders
} from '../../config'

// initial state
const state = {
  loading: false,
  ticket: {}
}

// getters
const getters = {
  fullDetails: state => state.ticket,
  replies: state => state.ticket.replies,
  id: state => state.ticket.id,
  tags: state => {
    return state.ticket.tags
  },
  agents: state => {
    return state.ticket.assignees
  },
  status: state => state.ticket.status.key,
  isLoading: state => state.loading
}

// actions
const actions = {

  getTicket({
    commit
  }, ticket) {
    var self = this;
    commit('toggleLoading');
    axios.get(endpoints.TicketServices.getTicket + '/' + ticket.id, {
        headers: getHeaders()
      })
      .then(function(response) {
        // commit mutations
        commit('setTicket', response.data.items)
        commit('toggleLoading');
      })
      .catch(function(error) {
        console.log(error);
      })
  },

  reloadDetails({
    commit
  }, ticket) {

    var self = this

    axios.get(endpoints.TicketServices.getTicket + '/' + ticket.id, {
        headers: getHeaders()
      })
      .then(function(response) {
        // commit mutations
        commit('setTicket', response.data.items)
      })
      .catch(function(error) {
        console.log(error);
      })

  },

  reloadReplies({
    commit
  }, ticket) {
    var self = this;

    axios.get(endpoints.TicketServices.getTicketReplies + '/' + ticket.id, {
        headers: getHeaders()
      })
      .then(function(response) {
        // commit mutations
        commit('setReplies', response.data.items.replies)
      })
      .catch(function(error) {
        console.log(error);
      })
  }

}

const mutations = {

  setTicket(state, ticket) {
    state.ticket = ticket
  },

  toggleLoading(state) {
    state.loading = !state.loading
  },

  setReplies(state, replies) {
    state.ticket.replies = replies
  }

}

export default {
  state,
  getters,
  actions,
  mutations
}
